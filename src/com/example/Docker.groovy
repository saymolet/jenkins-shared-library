#!/usr/bin/env groovy

package com.example

// implements Serializable - saving the state of the execution if the pipeline is paused and resumed
class Docker implements Serializable {
    def script // initialize a var

    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.sh "docker build -t $imageName ."
    }

    def dockerLogin(String dockerCredentialsInJenkins) {
        script.withCredentials([script.usernamePassword(credentialsId: "$dockerCredentialsInJenkins", usernameVariable: 'USER', passwordVariable: 'PASS')]) {
            script.sh "echo $script.PASS | docker login -u $script.USER --password-stdin" // better security.
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName"
    }

}