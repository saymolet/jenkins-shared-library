#!/usr/bin/env groovy

package com.example

// implements Serializable - saving the state of the execution if the pipeline is paused and resumed
class Git implements Serializable {
    def script // initialize a var

    Git(script) {
        this.script = script
    }

    def gitConfig(String email, username) {
        script.sh "git config user.email $email"
        script.sh "git config user.name $username"
    }

    def gitLoginRemote(String gitlabCredentialsInJenkins, gitlabRepo) {
        script.withCredentials([script.usernamePassword(credentialsId: "$gitlabCredentialsInJenkins", usernameVariable: 'USER', passwordVariable: 'PASS')]) {
            script.sh "git remote set-url origin https://$script.USER:$script.PASS@$gitlabRepo"
        }
    }

    def gitAddCommitPush(String branchName, message) {
        script.sh 'git add .'
        script.sh "git commit -m '$message'"
        script.sh "git push origin HEAD:$branchName"
    }

}