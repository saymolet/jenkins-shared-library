#!/usr/bin/env groovy
import com.example.Docker

def call(String imageName) {
    return new Docker(this).dockerPush(imageName)
    // "this" - passing all context from jenkins to Docker class
}