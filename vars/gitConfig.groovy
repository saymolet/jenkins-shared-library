#!/usr/bin/env groovy
import com.example.Git

def call(String email, username) {
    return new Git(this).gitConfig(email, username)
    // "this" - passing all context from jenkins to Git class
}