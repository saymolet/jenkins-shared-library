#!/usr/bin/env groovy
import com.example.Git

def call(String branchName, message) {
    return new Git(this).gitAddCommitPush(branchName, message)
}