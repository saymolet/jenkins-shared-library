#!/usr/bin/env groovy
import com.example.Git

def call(String gitlabCredentialsInJenkins, gitlabRepo) {
    return new Git(this).gitLoginRemote(gitlabCredentialsInJenkins, gitlabRepo)
}