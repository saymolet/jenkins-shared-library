#!/usr/bin/env groovy
import com.example.Docker

def call(String dockerCredentialsInJenkins) {
    return new Docker(this).dockerLogin(dockerCredentialsInJenkins)
    // "this" - passing all context from jenkins to Docker class
}